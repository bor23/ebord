#ifndef	_BUTTONS_H
#define	_BUTTONS_H 1

#include <gtk/gtk.h>

void press_home(void);
void press_prev(void);
void press_next(void);
void press_end(void);
void press_more(void);
void press_less(void);
void press_invert(GtkWidget *button, GtkWidget *draw_area);
void press_rag(GtkWidget *button, GtkWidget *draw_area);
void press_clear(GtkWidget *button, GtkWidget *draw_area);
void press_quit(void);

#endif
