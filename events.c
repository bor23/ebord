#include <cairo.h>
#include <gtk/gtk.h>
#include "buttons.h"
#include "draw.h"
#include "events.h"
#include "debug.h"

int first_conf = 0;
gint width_max = 0, height_max = 0;
int not_first_move = 0;
int flag_button_press = 0;
gint x_old, y_old;
extern cairo_surface_t *surface;
extern GdkPixbuf **page;

gboolean on_configure(GtkWidget *widget)
{
	DPRINT();
	gint width = gtk_widget_get_allocated_width(widget);
	gint height = gtk_widget_get_allocated_height(widget);
	if (width > width_max)
		width_max = width;
	if (height > height_max)
		height_max = height;
	GdkWindow *window = gtk_widget_get_window(widget);
	if (surface != NULL)
	{
		DPRINT("surface существует");
		if (first_conf)
		{
			DPRINT("первый запуск окна");
			cairo_surface_destroy(surface);
			surface = gdk_window_create_similar_surface(window, CAIRO_CONTENT_COLOR, width, height);
			draw_clear(widget);
			page[0] = gdk_pixbuf_get_from_surface(surface, 0, 0, width, height);
			first_conf = 0;
		}
		else
		{
			DPRINT("не первый запуск окна");
			page[0] = gdk_pixbuf_get_from_surface(surface, 0, 0, width_max, height_max);
			cairo_surface_destroy(surface);
			surface = gdk_cairo_surface_create_from_pixbuf(page[0], 0, window);
		}
	}
	else
	{
		DPRINT("surface не существует");
		surface = gdk_window_create_similar_surface(window, CAIRO_CONTENT_COLOR, width, height);
		draw_clear(widget);
		page[0] = gdk_pixbuf_get_from_surface(surface, 0, 0, width, height);
		first_conf = 1;
	}
	return TRUE;
}

gboolean on_draw(GtkWidget *widget, cairo_t *cr)
{
	DPRINT();
	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_paint(cr);
	return FALSE;
}

gboolean on_button_press(GtkWidget *widget, GdkEventButton *event)
{
	DPRINT();
	draw_brush(widget, event->x, event->y);
	x_old = event->x;
	y_old = event->y;
	not_first_move = 1;
	flag_button_press = 1;
	return TRUE;
}

gboolean on_button_release(GtkWidget *widget, GdkEventButton *event)
{
	DPRINT();
	flag_button_press = 0;
	return TRUE;
}
gboolean on_motion(GtkWidget *widget, GdkEventMotion *event)
{
	DPRINT();
	gint x, y;
	GdkModifierType state;
	if (event->is_hint)
	{
		DPRINT();
		gdk_window_get_device_position(event->window, event->device, &x, &y, &state);
	}
	else
	{
		DPRINT();
		x = event->x;
		y = event->y;
		state = event->state;
	}
	DPRINT("мышь движется");
	if (not_first_move && flag_button_press)
	{
		draw_brush(widget, x_old, y_old);
		draw_line_to(widget, x_old, y_old, x, y);
		draw_brush(widget, x, y);
	}
	x_old = x;
	y_old = y;
	not_first_move = 1;
	return TRUE;
}
