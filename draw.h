#ifndef _DRAW_H
#define _DRAW_H 1

#include <gtk/gtk.h>
#include <cairo.h>

void draw_clear(GtkWidget *draw_area);
void draw_brush(GtkWidget *widget, gint x, gint y);
void draw_line_to(GtkWidget *widget, gint x_old, gint y_old, gint x, gint y);

#endif
