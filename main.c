#include <cairo.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include "buttons.h"
#include "events.h"
#include "debug.h"
#include "draw.h"

#define FILE_NAME "/.ebordrc"

#define MAKE_BUTTON(parent, widget, name, label) \
	do { \
		GtkWidget *name = gtk_button_new_with_label(label); \
		gtk_widget_set_size_request(name, 70, 35); \
		gtk_box_pack_start(GTK_BOX(parent), name, TRUE, TRUE, 0); \
		g_signal_connect(G_OBJECT(name), "clicked", G_CALLBACK(press_##name), widget); \
	} while (0)

cairo_surface_t *surface;
GdkPixbuf **page;
extern int bw[2];
extern int thickness[2];

int main(int argc, char **argv)
{
	DPRINT();

	char settings_file[256];
	char *home_file = getenv("HOME");
	strcpy(settings_file, home_file);
	strcat(settings_file, FILE_NAME);
	FILE *f = fopen(settings_file, "r+");
	if (f != NULL)
		fscanf(f, "%d %d %d %d", &bw[0], &bw[1], &thickness[0], &thickness[1]);

	gtk_init(&argc, &argv);

	// Делаем окно
	GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(window), 1024, 768);
	gtk_window_set_title(GTK_WINDOW(window), "Программа для электронных досок");
	gtk_window_set_decorated(GTK_WINDOW(window), FALSE);

	// Делаем область для рисования
	GtkWidget *frame = gtk_frame_new(NULL);
	GtkWidget *draw_area = gtk_drawing_area_new();
	gtk_container_add(GTK_CONTAINER(frame), draw_area);

	// Делаем область с кнопками
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_widget_set_halign(hbox, GTK_ALIGN_CENTER);
	//MAKE_BUTTON(hbox, draw_area, home, "Начало");
	//MAKE_BUTTON(hbox, draw_area, prev, "Предыдущая");
	//MAKE_BUTTON(hbox, draw_area, next, "Следующая");
	//MAKE_BUTTON(hbox, draw_area, end, "Конец");
	MAKE_BUTTON(hbox, draw_area, more, "Толще");
	MAKE_BUTTON(hbox, draw_area, less, "Тоньше");
	MAKE_BUTTON(hbox, draw_area, invert, "Инвертировать");
	MAKE_BUTTON(hbox, draw_area, rag, "Тряпка");
	MAKE_BUTTON(hbox, draw_area, clear, "Очистить");
	MAKE_BUTTON(hbox, window, quit, "Выход");

	// Компонуем предыдущие две области в окно
	GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	//Создаём первую пиксельный буфер для первой страницы
	page = malloc(sizeof(GdkPixbuf *));

	//Обработка событий
	gtk_widget_set_events(draw_area, gtk_widget_get_events(draw_area) | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_MOTION_MASK | GDK_POINTER_MOTION_MASK);
	g_signal_connect(G_OBJECT(draw_area), "draw", G_CALLBACK(on_draw), NULL);
	g_signal_connect(G_OBJECT(draw_area), "button-press-event", G_CALLBACK(on_button_press), NULL);
	g_signal_connect(G_OBJECT(draw_area), "button-release-event", G_CALLBACK(on_button_release), NULL);
	g_signal_connect(G_OBJECT(draw_area), "motion-notify-event", G_CALLBACK(on_motion), NULL);
	g_signal_connect(G_OBJECT(draw_area), "configure-event", G_CALLBACK(on_configure), NULL);
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

	// Показываем всё, что получилось
	gtk_widget_show_all(window);

	gdk_window_set_cursor(gtk_widget_get_window(window), gdk_cursor_new(GDK_BLANK_CURSOR));
	gdk_window_move(gtk_widget_get_window(window), 2000, 0);
	gtk_window_maximize(GTK_WINDOW(window));

	// Основной цикл
	gtk_main();

	cairo_surface_destroy(surface);

	if (f == NULL)
		f = fopen(settings_file, "w");
	rewind(f);
	fprintf(f, "%d %d %d %d", bw[0], bw[1], thickness[0], thickness[1]);
	fclose(f);

	free(page);
	return 0;
}
