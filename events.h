#ifndef _EVENTS_H
#define _EVENTS_H 1

#include <gtk/gtk.h>
#include <cairo.h>

gboolean on_configure(GtkWidget *widget);
gboolean on_draw(GtkWidget *widget, cairo_t *cr);
gboolean on_button_press(GtkWidget *widget, GdkEventButton *event);
gboolean on_button_release(GtkWidget *widget, GdkEventButton *event);
gboolean on_motion(GtkWidget *widget, GdkEventMotion *event);

#endif
