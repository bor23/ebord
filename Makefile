BIN=ebord
CC=gcc
CFLAGS=`pkg-config --cflags gtk+-3.0`
LDFLAGS=`pkg-config --libs gtk+-3.0`

$(BIN): main.o buttons.o events.o draw.o
	$(CC) $(LDFLAGS) -o $(BIN) -O2 $^

%.o: %.c
	$(CC) $(CFLAGS) -c -O2 $<

clean:
	rm -f *~ *.o $(BIN)
