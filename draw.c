#include <cairo.h>
#include <gtk/gtk.h>
#include <math.h>
#include "buttons.h"
#include "draw.h"
#include "debug.h"

extern cairo_surface_t *surface;
extern int rag;
extern int bw[2];
extern int thickness[2];

void draw_clear(GtkWidget *draw_area)
{
	DPRINT();
	cairo_t *cr = cairo_create(surface);
	cairo_set_source_rgb(cr, bw[1], bw[1], bw[1]);
	cairo_paint(cr);
	cairo_destroy(cr);
	gtk_widget_queue_draw(draw_area);
}

void draw_brush(GtkWidget *widget, gint x, gint y)
{
	DPRINT();
	cairo_t *cr = cairo_create(surface);
	cairo_set_source_rgb(cr, bw[rag], bw[rag], bw[rag]);
	cairo_arc(cr, x, y, thickness[rag] / 2, 0, 2 * M_PI);
	cairo_fill(cr);
	cairo_destroy(cr);
	gtk_widget_queue_draw_area(widget, x - thickness[rag] / 2, y - thickness[rag] / 2, thickness[rag], thickness[rag]);
}

void draw_line_to(GtkWidget *widget, gint x_old, gint y_old, gint x, gint y)
{
	DPRINT();
	cairo_t *cr = cairo_create(surface);
	cairo_set_source_rgb(cr, bw[rag], bw[rag], bw[rag]);
	cairo_set_line_width(cr, thickness[rag]);
	cairo_move_to(cr, x_old, y_old);
	cairo_line_to(cr, x, y);
	cairo_stroke(cr);
	cairo_destroy(cr);
	gtk_widget_queue_draw(widget);
}
