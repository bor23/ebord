#include <gtk/gtk.h>
#include "buttons.h"
#include "draw.h"
#include "debug.h"

int page_num = 1;
int last_page_num = 1;
int rag = 0;
int bw[] = {0, 1};
int thickness[] = {7, 57};

void press_home(void)
{
	DPRINT();
	page_num = 1;
}

void press_end(void)
{
	DPRINT();
	page_num = last_page_num;
}

void press_prev(void)
{
	DPRINT();
	if (page_num > 1)
		page_num--;
}

void press_next(void)
{
	DPRINT();
	page_num++;
	if (page_num > last_page_num)
	{
		last_page_num = page_num;
		DPRINT("создана новая страница");
	}
}

void press_less(void)
{
	DPRINT();
	thickness[rag]--;
	if (thickness[rag] < 1)
		thickness[rag] = 1;
}

void press_more(void)
{
	DPRINT();
	thickness[rag]++;
}

void press_invert(GtkWidget *button, GtkWidget *draw_area)
{
	DPRINT();
	bw[0] ^= 1;
	bw[1] ^= 1;
	gtk_widget_queue_draw(draw_area);
}

void press_rag(GtkWidget *button, GtkWidget *draw_area)
{
	DPRINT();
	rag ^= 1;
	if (rag)
		gtk_button_set_label(GTK_BUTTON(button), "Линия");
	else
		gtk_button_set_label(GTK_BUTTON(button), "Тряпка");
	gtk_widget_queue_draw(button);
}

void press_clear(GtkWidget *button, GtkWidget *draw_area)
{
	DPRINT();
	draw_clear(draw_area);
}

void press_quit(void)
{
	DPRINT();
	gtk_main_quit();
}
