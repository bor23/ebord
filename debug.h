#ifndef _DEBUG_H
#define _DEBUG_H 1

#include <gtk/gtk.h>
#define DEBUG 0
#if DEBUG
#	define DPRINT(str) g_print("%u: %s: "str"\n", __LINE__, __func__);
#else
#	define DPRINT(str)
#endif

#endif
